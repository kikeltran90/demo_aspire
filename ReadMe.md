Setup Java, Maven, Allure environments

Extract the zip file

Configure environment value in demo.properties

Open terminal
- Run command 'mvn clean install' //Build and Install Dependencies
- Run command 'mvn clean test' //Run the test
- Run command 'allure serve allure-results' //View the report
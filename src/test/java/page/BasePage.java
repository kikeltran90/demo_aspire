package page;

import helper.PropertiesHelper;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.Random;

public abstract class BasePage {

    protected static final String DEMO1_DATA = "demo.properties";
    protected static WebDriver webDriver;
    protected String timeWait = PropertiesHelper.getConfigValue(DEMO1_DATA, "timeWait");

    /**
     * Constructor
     * @param webDriver
     */
    public BasePage(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(this.webDriver, this);
    }

    /**
     * Input text
     */
    public void inputText(WebElement webElement, String text) {
        try {
            WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(Long.parseLong(timeWait)));
            wait.until(ExpectedConditions.visibilityOf(webElement));
            clearText(webElement);
            webElement.sendKeys(text);
        } catch (TimeoutException e) {
            System.out.println("No such element exists");
        }
    }

    /**
     * Clear text
     */
    public void clearText(WebElement webElement){
        try {
            WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(Long.parseLong(timeWait)));
            wait.until(ExpectedConditions.visibilityOf(webElement));
            webElement.clear();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    /**
     * Click button
     */
    public void buttonClick(WebElement webElement) {
        try {
            WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(Long.parseLong(timeWait)));
            wait.until(ExpectedConditions.elementToBeClickable(webElement));
            hoverElement(webElement);
            webElement.click();
        } catch (TimeoutException e) {
            System.out.println("No element to click");
        }
    }

    /**
     * Click button js
     */
    public void buttonClickJs(WebElement webElement) {
        try {
            WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(Long.parseLong(timeWait)));
            wait.until(ExpectedConditions.elementToBeClickable(webElement));
            JavascriptExecutor executor = (JavascriptExecutor)webDriver;
            executor.executeScript("arguments[0].click();", webElement);
        } catch (TimeoutException e) {
            System.out.println("No element to click");
        }
    }

    /**
     * Hover element
     */
    public void hoverElement(WebElement webElement) {
        try {
            Actions actions = new Actions(webDriver);
            actions.moveToElement(webElement).build().perform();
        } catch (TimeoutException e) {
            System.out.println("No element to hover");
        }
    }

    /**
     * GET Element text
     */
    public String getText(WebElement webElement) {
        if (webElement.getTagName().equals("input") || webElement.getTagName().equals("textarea")) {
            return webElement.getAttribute("value");
        }else {
            return webElement.getText();
        }
    }

    /**
     * Select Random option in dropdown
     */
    public void selectRandomOptionDropdown(WebElement dropdown) {
        try {
            waitPageLoad();
            WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(Long.parseLong(timeWait)));
            wait.until(ExpectedConditions.visibilityOf(dropdown));
            Select select = new Select(dropdown);
            List<WebElement> options = select.getOptions();
            if (options.size() == 0)
                System.out.println("No option to choose");
            if (options.size() == 1)
                select.selectByIndex(0);
            else {
                int optionIndex = getRandomNumberInRange(0, options.size() - 1);
                select.selectByIndex(optionIndex);
            }
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get Random number in range
     */
    public int getRandomNumberInRange(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

    /**
     * Wait for javascript to load
     */
    public boolean waitPageLoad() {
        WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(Long.parseLong(timeWait)));
        // wait for jQuery to load
        ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return ((Long) ((JavascriptExecutor) driver).executeScript("return jQuery.active") == 0);
                } catch (Exception e) {
                    // no jQuery present
                    return true;
                }
            }
        };

        // wait for Javascript to load
        ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState")
                        .toString().equals("complete");
            }
        };

        return wait.until(jQueryLoad) && wait.until(jsLoad);
    }

    /**
     * Wait for element to be visible
     */
    public void waitUntilElementVisible(WebElement element){
        WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(Long.parseLong(timeWait)));
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    /**
     * Wait for element to be clickable
     */
    public void waitUntilElementClickable(WebElement element){
        WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(Long.parseLong(timeWait)));
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    /**
     * Wait for element to be invisible
     */
    public void waitUntilElementInvisible(WebElement element){
        WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(Long.parseLong(timeWait)));
        wait.until(ExpectedConditions.invisibilityOf(element));
    }

    /**
     * Wait for element to be invisible
     */
    public void waitUntilTitleContains(String title){
        WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(Long.parseLong(timeWait)));
        wait.until(ExpectedConditions.titleContains(title));
    }

    /**
     * Wait for element to be invisible
     */
    public void waitUntilTextPresent(WebElement element, String text){
        WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(Long.parseLong(timeWait)));
        wait.until(ExpectedConditions.textToBePresentInElement(element, text));
    }

    /**
     * Thread Sleep
     */
    public void threadSleep(int seconds) {
        try {
            Thread.sleep((long)(seconds * 1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}

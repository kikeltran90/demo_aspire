package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class InventoryPage extends BasePage{

    @FindBy(css = "button[title='Products']")
    private WebElement inventory_dropdownProducts;

    @FindBy(css = "button[title='Products']+div a:first-child")
    private WebElement inventory_dropdownItemProducts;

    @FindBy(css = "a[title='Home menu']")
    private WebElement inventory_iconApplication;

    /**
     * Constructor
     *
     * @param webDriver
     */
    public InventoryPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void openProductPage() {
        buttonClick(inventory_dropdownProducts);
        buttonClick(inventory_dropdownItemProducts);
        waitUntilTitleContains("Products");
    }

    public void clickApplicationIcon() {
        buttonClick(inventory_iconApplication);
    }

}

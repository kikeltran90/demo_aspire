package page;

import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class ProductPage extends BasePage{

    @FindBy(css = "button[title='Create record']")
    private WebElement product_btnCreateProduct;

    @FindBy(css = "input[name='name']")
    private WebElement product_txtProductName;

    @FindBy(css = "select[name='detailed_type']")
    private WebElement product_dropdownProductType;

    @FindBy(css = "div[name='list_price'] input")
    private WebElement product_txtPrice;

    @FindBy(css = "div[name='standard_price'] input")
    private WebElement product_txtCost;

    @FindBy(css = "input[name='default_code']")
    private WebElement product_txtInternalRef;

    @FindBy(css = "input[name='barcode'][type='text']")
    private WebElement product_txtBarcode;

    @FindBy(css = ".oe-collaboration-selections-container+div p")
    private WebElement product_txtInternalNote;

    @FindBy(xpath = "//a[@role='tab'][normalize-space()='Inventory']")
    private WebElement product_labelInventoryTab;

    @FindBy(xpath = "//label[normalize-space()='Manufacture']")
    private WebElement product_labelManufacture;

    @FindBy(css = "input[name='weight']")
    private WebElement product_txtWeight;

    @FindBy(css = "input[name='volume']")
    private WebElement product_txtVolume;

    @FindBy(css = "input[name='produce_delay']")
    private WebElement product_txtManLeadTime;

    @FindBy(css = "input[name='sale_delay']")
    private WebElement product_txtCusLeadTime;

    @FindBy(css = "textarea[name='description_pickingin']")
    private WebElement product_txtDescReceipts;

    @FindBy(css = "textarea[name='description_pickingout']")
    private WebElement product_txtDescDelivery;

    @FindBy(css = "textarea[name='description_picking']")
    private WebElement product_txtInternalTransfer;

    @FindBy(xpath = "//button[normalize-space()='Save']")
    private WebElement product_btnSaveProduct;

    @FindBy(css = ".o_FollowButton_follow")
    private WebElement product_btnFollow;

    @FindBy(css = "button[name='action_update_quantity_on_hand']")
    private WebElement product_btnUpdateQuantity;

    @FindBy(css = "button[name='action_update_quantity_on_hand']")
    private WebElement product_imgNoContentSmilingFace;

    @FindBy(css = "button[data-original-title='Create record']")
    private WebElement product_btnCreateQuantity;

    @FindBy(css = "div[name='location_id'] .o_field_many2one_selection")
    private WebElement product_dropdownLocation;

    @FindBy(css = "input[name='inventory_quantity']")
    private WebElement product_txtCountedQuantity;

    @FindBy(css = "span[name='inventory_diff_quantity']")
    private WebElement product_labelQuantityDiff;

    @FindBy(css = "button[title='Save record']")
    private WebElement product_btnSaveQuantity;

    @FindBy(xpath = "//p[normalize-space()='Product Template created']")
    private WebElement product_labelSuccessMsg;

    @FindBy(css = "li[title='Previous menu'][class='breadcrumb-item o_back_button']")
    private WebElement product_linkBreadcrumbProduct;

    /**
     * Constructor
     *
     * @param webDriver
     */
    public ProductPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void clickCreateBtn() {
        buttonClick(product_btnCreateProduct);
    }

    @Step("Fill in Product General Information")
    public void inputProductGeneralInfo(String productName, String productType, String price, String cost, String internalRef, String barcode, String internalNote) {
        waitUntilElementVisible(product_txtProductName);
        //input product name
        inputText(product_txtProductName, productName);
        //select product type
        Select dropdownProductType = new Select(product_dropdownProductType);
        dropdownProductType.selectByVisibleText(productType);
        //input price
        inputText(product_txtPrice, price);
        //input cost
        inputText(product_txtCost, cost);
        //input Internal Reference
        inputText(product_txtInternalRef, internalRef);
        //input barcode
        inputText(product_txtBarcode, barcode);
        //input Internal Reference
        inputText(product_txtInternalNote, internalNote);
    }

    @Step("Fill in Product Inventory Information")
    public void inputProductInventoryInfo(String weight, String volume, String manLeadTime, String cusLeadTime, String note) {
        //select Inventory tab
        buttonClick(product_labelInventoryTab);
        //tick Manufacture checkbox
        buttonClick(product_labelManufacture);
        //input weight
        inputText(product_txtWeight, weight);
        //input volume
        inputText(product_txtVolume, volume);
        //input Manu Lead Time
        inputText(product_txtManLeadTime, manLeadTime);
        //input Cus Lead Time
        inputText(product_txtCusLeadTime, cusLeadTime);
        //input Desc Receipts
        inputText(product_txtDescReceipts, note);
        //input Desc Delivery
        inputText(product_txtDescDelivery, note);
        //input Internal Transfer
        inputText(product_txtInternalTransfer, note);
        buttonClick(product_btnSaveProduct);
        waitUntilElementInvisible(product_btnSaveProduct);
        waitUntilElementInvisible(product_btnFollow);
    }

    @Step("Update Quantity")
    public void updateQuantity(String quantity) {
        //click Update Quantity button
        buttonClick(product_btnUpdateQuantity);
        waitPageLoad();
        //click Create Quantity
        buttonClick(product_btnCreateQuantity);
        //Select location
        waitUntilElementClickable(product_dropdownLocation);
        buttonClick(product_dropdownLocation);
        waitPageLoad();
        //Input Counted Quantity
        inputText(product_txtCountedQuantity, quantity);
        buttonClick(product_labelQuantityDiff);
        waitUntilTextPresent(product_labelQuantityDiff, quantity);
        buttonClick(product_btnSaveQuantity);
        buttonClick(product_linkBreadcrumbProduct);
        waitUntilElementVisible(product_labelInventoryTab);
    }

    @Step("Verify if Product is created successfully")
    public boolean verifyProductCreated() {
        waitPageLoad();
        boolean isPresent = false;
        if (product_labelSuccessMsg.isDisplayed())
            isPresent = true;
        return isPresent;
    }


}

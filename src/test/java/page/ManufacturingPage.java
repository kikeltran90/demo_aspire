package page;

import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class ManufacturingPage extends BasePage{

    @FindBy(css = "button[data-original-title='Create record']")
    private WebElement manu_btnCreateManu;

    @FindBy(css = "button[name='action_confirm']")
    private WebElement manu_btnConfirm;

    @FindBy(xpath = "//div[@name='product_id']//div[@class='o_input_dropdown']//input")
    private WebElement manu_txtProduct;

    @FindBy(xpath = "//div[@name='bom_id']//input")
    private WebElement manu_txtBoM;

    @FindBy(xpath = "//div[@name='bom_id']//a")
    private WebElement manu_btnDropdownBoM;

    @FindBy(css = ".modal-body")
    private WebElement manu_tabBoM;

    @FindBy(xpath = "//span[normalize-space()='Create']")
    private WebElement manu_btnCreateBoM;

    @FindBy(xpath = "//span[normalize-space()='Save']")
    private WebElement manu_btnSaveBoM;

    @FindBy(css = "div[name='move_raw_ids'] .o_field_x2many_list_row_add a")
    private WebElement manu_linkAddLineComponent;

    @FindBy(css = "div[name='move_raw_ids'] input")
    private WebElement manu_txtComponentProduct;

    @FindBy(xpath = "//li[@class='nav-item']/a[normalize-space()='Miscellaneous']")
    private WebElement manu_tabMiscellaneous;

    @FindBy(css = "input[name='origin']")
    private WebElement manu_txtSource;

    @FindBy(css = "button[title='Save record']")
    private WebElement manu_btnSave;

    @FindBy(xpath = "//button[@class='btn btn-primary']//span[contains(text(),'Mark as Done')]")
    private WebElement manu_btnMarkDone;

    @FindBy(css = "button[name='process']")
    private WebElement manu_btnApply;

    @FindBy(css = "button[data-value='done'][title='Current state']")
    private WebElement manu_labelActiveDoneState;

    @FindBy(css = "a[name='product_id'] span")
    private WebElement manu_fieldProduct;

    @FindBy(css = "a[name='bom_id'] span")
    private WebElement manu_fieldBoM;

    @FindBy(xpath = "//td[@name='product_id']")
    private WebElement manu_fieldComponentProduct;

    @FindBy(css = "span[name='origin']")
    private WebElement manu_fieldSource;

    @FindBy(xpath = "//p[normalize-space()='Production Order created']")
    private WebElement manu_labelSuccessMsg;

    /**
     * Constructor
     *
     * @param webDriver
     */
    public ManufacturingPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void clickCreateBtn() {
        buttonClick(manu_btnCreateManu);
    }

    @Step("Create Manufacturing Order")
    public void createManufacturingOrder(String productName) {
        waitUntilElementVisible(manu_btnConfirm);
        //fill in Product info
        inputText(manu_txtProduct, productName);
        waitPageLoad();
        manu_txtProduct.sendKeys(Keys.ENTER);
        //fill in Bill Material
        threadSleep(2);
        inputText(manu_txtBoM, productName);
        threadSleep(2);
        manu_txtBoM.sendKeys(Keys.ENTER);
        waitUntilElementVisible(manu_tabBoM);
//        if (manu_btnCreateBoM.isDisplayed()) {
//            buttonClickJs(manu_btnCreateBoM);
//        }
        waitPageLoad();
        buttonClickJs(manu_btnSaveBoM);
        threadSleep(2);
        waitPageLoad();

        //add line Component
        buttonClick(manu_linkAddLineComponent);
        inputText(manu_txtComponentProduct, productName);
        waitPageLoad();
        threadSleep(2);
        manu_txtComponentProduct.sendKeys(Keys.ENTER);
        //Miscellaneous tab
        buttonClick(manu_tabMiscellaneous);
        inputText(manu_txtSource, productName);
        buttonClick(manu_btnSave);
        waitUntilElementInvisible(manu_btnSave);
//        waitUntilTitleContains("WH/MO");
    }

    @Step("Verify if Manufacturing Order is created successfully")
    public boolean verifyManufacturingOrderCreated() {
        waitPageLoad();
        boolean isCreated = false;
        if (webDriver.getTitle().contains("WH/MO"))
            isCreated = true;
        return isCreated;
    }

    @Step("Update Manufacturing Order to Done")
    public void updateManuOrderDone() {
        //Click Confirm
        buttonClick(manu_btnConfirm);
        //Click Mark Done
        buttonClick(manu_btnMarkDone);
        //Click Apply
        buttonClick(manu_btnApply);
        waitUntilElementVisible(manu_labelActiveDoneState);
    }

    @Step("Validate created Manufacturing Order")
    public void checkManufacturingOrder(String productName) {
        //Check Product
        Assert.assertEquals(manu_fieldProduct.getText(), productName);
        //Check BoM
        Assert.assertEquals(manu_fieldBoM.getText(), productName);
        //Check Component Product
        System.out.println("ComPro: " + manu_fieldComponentProduct.getText());
        Assert.assertTrue(productName.contains(manu_fieldComponentProduct.getText()));
        buttonClick(manu_tabMiscellaneous);
        //Check Source
        System.out.println("Source: " + manu_fieldSource.getText());
        Assert.assertTrue(productName.contains(manu_fieldSource.getText()));
    }




}

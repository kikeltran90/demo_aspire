package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NavigationPage extends BasePage{

    @FindBy(css = "#result_app_1")
    private WebElement navigation_inventoryItem;

    @FindBy(css = "#result_app_2")
    private WebElement navigation_manufacturingItem;

    @FindBy(xpath = "//a[normalize-space()='Inventory']")
    private WebElement inventory_inventoryBrand;

    @FindBy(xpath = "//a[normalize-space()='Manufacturing']")
    private WebElement manufacturing_manufacturingBrand;

    /**
     * Constructor
     *
     * @param webDriver
     */
    public NavigationPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void navigateToInventoryPage() {
        buttonClick(navigation_inventoryItem);
        waitUntilElementVisible(inventory_inventoryBrand);
    }

    public void navigateToManufacturingPage() {
        buttonClick(navigation_manufacturingItem);
        waitUntilElementVisible(manufacturing_manufacturingBrand);
    }


}

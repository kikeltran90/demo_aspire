package page;

import helper.PropertiesHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {

    @FindBy(css = "#login")
    private WebElement login_txtUsername;

    @FindBy(css = "#password")
    private WebElement login_txtPassword;

    @FindBy(css = ".oe_login_buttons")
    private WebElement login_btnLogIn;

    @FindBy(css = ".o_user_avatar")
    private WebElement navigation_avatarUser;


    /**
     * Constructor
     *
     * @param webDriver
     */
    public LoginPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void loginToApp(String username, String password) {
        inputText(login_txtUsername, username);
        inputText(login_txtPassword, password);
        buttonClick(login_btnLogIn);
        waitUntilElementVisible(navigation_avatarUser);
    }

}

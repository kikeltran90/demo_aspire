package data;

import java.util.Random;

public class ProductData {

    //General Information
    private String p_Name;
    private String p_Type;
    private String p_Price;
    private String p_Cost;
    private String p_InternalRef;
    private String p_Barcode;
    private String p_InternalNote;

    //Inventory Information
    private String p_Weight;
    private String p_Volume;
    private String p_MLT;
    private String p_CLT;

    //Update Quantity
    private String p_Quantity;

    public ProductData(String p_Name, String p_Type, String p_Price, String p_Cost, String p_InternalRef, String p_Barcode,
                      String p_InternalNote, String p_Weight, String p_Volume, String p_MLT, String p_CLT, String p_Quantity) {
        super();
        this.p_Name = p_Name;
        this.p_Type = p_Type;
        this.p_Price = p_Price;
        this.p_Cost = p_Cost;
        this.p_InternalRef = p_InternalRef;
        this.p_Barcode = p_Barcode;
        this.p_InternalNote = p_InternalNote;
        this.p_Weight = p_Weight;
        this.p_Volume = p_Volume;
        this.p_MLT = p_MLT;
        this.p_CLT = p_CLT;
        this.p_Quantity = p_Quantity;
    }


    public String getSystemProductName(){
        return "[" + p_InternalRef + "] " + p_Name;
    }

    public static long randomBarcode() {
        long barcode = (long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L;
        return barcode;
    }

    public static int getRandomNumberInRange(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

    public String getP_Name() {
        return p_Name;
    }

    public void setP_Name(String p_Name) {
        this.p_Name = p_Name;
    }

    public String getP_Type() {
        return p_Type;
    }

    public void setP_Type(String p_Type) {
        this.p_Type = p_Type;
    }

    public String getP_Price() {
        return p_Price;
    }

    public void setP_Price(String p_Price) {
        this.p_Price = p_Price;
    }

    public String getP_Cost() {
        return p_Cost;
    }

    public void setP_Cost(String p_Cost) {
        this.p_Cost = p_Cost;
    }

    public String getP_InternalRef() {
        return p_InternalRef;
    }

    public void setP_InternalRef(String p_InternalRef) {
        this.p_InternalRef = p_InternalRef;
    }

    public String getP_Barcode() {
        return p_Barcode;
    }

    public void setP_Barcode(String p_Barcode) {
        this.p_Barcode = p_Barcode;
    }

    public String getP_InternalNote() {
        return p_InternalNote;
    }

    public void setP_InternalNote(String p_InternalNote) {
        this.p_InternalNote = p_InternalNote;
    }

    public String getP_Weight() {
        return p_Weight;
    }

    public void setP_Weight(String p_Weight) {
        this.p_Weight = p_Weight;
    }

    public String getP_Volume() {
        return p_Volume;
    }

    public void setP_Volume(String p_Volume) {
        this.p_Volume = p_Volume;
    }

    public String getP_MLT() {
        return p_MLT;
    }

    public void setP_MLT(String p_MLT) {
        this.p_MLT = p_MLT;
    }

    public String getP_CLT() {
        return p_CLT;
    }

    public void setP_CLT(String p_CLT) {
        this.p_CLT = p_CLT;
    }

    public String getP_Quantity() {
        return p_Quantity;
    }

    public void setP_Quantity(String p_Quantity) {
        this.p_Quantity = p_Quantity;
    }

}

package data;

import java.util.Random;

public class Data {

    //General Information
    public static String productName = "Demo" + System.currentTimeMillis();
    public static String productType = "Storable Product";
    public static String price = Integer.toString(getRandomNumberInRange(1, 10));
    public static String cost = Integer.toString(getRandomNumberInRange(1, 10));
    public static String internalRef = "Demo Ref" + System.currentTimeMillis();
    public static String barcode = Long.toString(randomBarcode());
    public static String internalNote = "Demo Note";

    //Inventory Information
    public static String weight = Integer.toString(getRandomNumberInRange(1, 10));
    public static String volume = Integer.toString(getRandomNumberInRange(1, 10));
    public static String manLeadTime = Integer.toString(getRandomNumberInRange(1, 10));
    public static String cusLeadTime = Integer.toString(getRandomNumberInRange(1, 10));

    //Update Quantity
    public static String quantity = Integer.toString(getRandomNumberInRange(11, 100));

    //Manufacturing Information

    public String getSystemProductName(){
        return "[" + internalRef + "] " + productName;
    }

    public static long randomBarcode() {
        long barcode = (long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L;
        return barcode;
    }

    public static int getRandomNumberInRange(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

}

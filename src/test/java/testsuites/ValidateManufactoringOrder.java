package testsuites;

import data.Data;
import helper.PropertiesHelper;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Description;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.*;
import page.*;

import java.util.concurrent.TimeUnit;

public class ValidateManufactoringOrder {

    protected static WebDriver webDriver;
    protected static final String DEMO_DATA = "demo.properties";
    protected String url = PropertiesHelper.getConfigValue(DEMO_DATA, "url");
    protected String browser = PropertiesHelper.getConfigValue(DEMO_DATA, "browser");
    protected String username = PropertiesHelper.getConfigValue(DEMO_DATA, "username");
    protected String password = PropertiesHelper.getConfigValue(DEMO_DATA, "password");
    protected String headless = PropertiesHelper.getConfigValue(DEMO_DATA, "headless");
    LoginPage loginPage;
    InventoryPage inventoryPage;
    NavigationPage navigationPage;
    ProductPage productPage;
    ManufacturingPage manufacturingPage;
    Data data = new Data();

    enum BrowserType {
        CHROME, FIREFOX, IE
    }

    public void chooseBrowser() {
        BrowserType type = BrowserType.valueOf(browser.toUpperCase());
        boolean mode = Boolean.parseBoolean(headless);
        switch (type) {
            case CHROME:
                WebDriverManager.chromedriver().setup();
                ChromeOptions options = new ChromeOptions();
                options.setHeadless(mode);
                webDriver = new ChromeDriver(options);
                break;
            case FIREFOX:
                WebDriverManager.firefoxdriver().setup();
                webDriver = new FirefoxDriver();
                break;
            case IE:
                WebDriverManager.iedriver().setup();
                webDriver = new InternetExplorerDriver();
                break;
            default:
                break;
        }
    }

    @BeforeTest
    public void setUp(){
        chooseBrowser();
        webDriver.manage().window().maximize();
        webDriver.get(url);
    }

    @Test(priority = 0)
    @Description("Verify if user can login successfully")
    public void test_LoginToApplication(){
        loginPage = new LoginPage(webDriver);

        loginPage.loginToApp(username, password);
    }

    @Test(priority = 1)
    @Description("Verify if user can create product successfully")
    public void test_CreateProduct() throws InterruptedException {
        navigationPage = new NavigationPage(webDriver);
        inventoryPage = new InventoryPage(webDriver);
        productPage = new ProductPage(webDriver);

        navigationPage.navigateToInventoryPage();
        inventoryPage.openProductPage();
        productPage.clickCreateBtn();
        productPage.inputProductGeneralInfo(data.productName, data.productType, data.price, data.cost, data.internalRef, data.barcode, data.internalNote);
        productPage.inputProductInventoryInfo(data.weight, data.volume, data.manLeadTime, data.cusLeadTime, data.internalNote);
        productPage.verifyProductCreated();
    }

    @Test(priority = 2)
    @Description("Verify user can update quantity of a product successfully")
    public void test_UpdateQuantity() throws InterruptedException {
        productPage = new ProductPage(webDriver);

        productPage.updateQuantity(data.quantity);
    }

    @Test(priority = 3)
    @Description("Verify if user can update quantity of a product successfully")
    public void test_CreateManufacturingOrder() {
        inventoryPage = new InventoryPage(webDriver);
        navigationPage = new NavigationPage(webDriver);
        manufacturingPage = new ManufacturingPage(webDriver);

        inventoryPage.clickApplicationIcon();
        navigationPage.navigateToManufacturingPage();
        manufacturingPage.clickCreateBtn();
        manufacturingPage.createManufacturingOrder(data.productName);
        manufacturingPage.verifyManufacturingOrderCreated();
    }

    @Test(priority = 4)
    @Description("Verify if user can update Manufacturing Order to Done status successfully")
    public void test_UpdateDoneManufacturingOrder() {
        manufacturingPage = new ManufacturingPage(webDriver);

        manufacturingPage.updateManuOrderDone();
    }

    @Test(priority = 5)
    @Description("Verify if user can update Manufacturing Order to Done status successfully")
    public void test_ValidateManufacturingOrder() {
        manufacturingPage = new ManufacturingPage(webDriver);

        manufacturingPage.checkManufacturingOrder(data.getSystemProductName());
    }

    @AfterTest
    public static void tearDown() {
        webDriver.close();
        webDriver.quit();
        webDriver = null;
    }
}
